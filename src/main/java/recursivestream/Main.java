package recursivestream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import lombok.val;

import static java.lang.System.out;
import static java.nio.file.Files.isDirectory;
import static java.nio.file.Files.newDirectoryStream;
import static ru.petrovandrei.commons.datamanagement.Closers.close;
import static ru.petrovandrei.commons.datamanagement.Closers.close_andRethrowWithoutCompilerChecking;
import static ru.petrovandrei.commons.datamanagement.IterableUtil.asStream;

public class Main {

    public static void main(String[] args) throws IOException {
        val sum = recursiveStream(Paths.get("D:\\"))
                .mapToLong(path -> sizeOf(path))
                .reduce((a, b) -> a + b);

        out.println("sum: " + sum);
    }

    static Stream<Path> recursiveStream(Path root) throws IOException {
        val directoryStream = newDirectoryStream(root);
        return asStream(directoryStream)
//                .filter(path -> !isDirectory(path) || path.getFileName().endsWith("System Volume Information"))
                .flatMap(path -> {
                    try {
                        return isDirectory(path)
                                ? recursiveStream(path)
                                : Stream.of(path);
                    } catch (Throwable reason) {
                        close_andRethrowWithoutCompilerChecking(directoryStream, reason);

                        //<editor-fold defaultstate="collapsed" desc="will not be executed">
                        //needed only for deceived compiler
                        return null;
                        //</editor-fold>
                    }
                })
                .onClose(() -> close(directoryStream));
    }

    @SneakyThrows
    static long sizeOf(Path path) {
        return Files.size(path);
    }
}