package ru.petrovandrei.commons;

public class ThrowableUtil {

    @SuppressWarnings("unchecked")
    public static <E extends Throwable> void throwAsUnchecked(Throwable e) throws E {
        throw (E) e;
    }
}