package ru.petrovandrei.commons.datamanagement;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class IterableUtil {

    public static <T> Stream<T> asStream(Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}