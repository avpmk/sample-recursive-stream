package ru.petrovandrei.commons.datamanagement;

import static ru.petrovandrei.commons.ThrowableUtil.throwAsUnchecked;

public class Closers {

    public static void close(AutoCloseable that) {
        try {
            that.close();
        } catch (Exception e) {
            throwAsUnchecked(e);
        }
    }

    public static void close_andRethrowWithoutCompilerChecking(
            AutoCloseable that, Throwable reason
    ) {
        try {
            that.close();
        } catch (Throwable th) {
            reason.addSuppressed(th);
        }
        throwAsUnchecked(reason);
    }
}